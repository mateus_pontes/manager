//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Manager.Painel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cliente
    {
        public Cliente()
        {
            this.OrdemServicoes = new HashSet<OrdemServico>();
        }
    
        public int Codigo { get; set; }
        public Nullable<int> CodigoEmpresa { get; set; }
        public Nullable<int> CodigoClienteTipo { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Cep { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Site { get; set; }
        public string Pessoa { get; set; }
        public string Observacao { get; set; }
        public System.DateTime DtCadastro { get; set; }
    
        public virtual Empresa Empresa { get; set; }
        public virtual ICollection<OrdemServico> OrdemServicoes { get; set; }
    }
}
