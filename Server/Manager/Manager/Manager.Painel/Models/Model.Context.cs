﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Manager.Painel.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class db_Entities : DbContext
    {
        public db_Entities()
            : base("name=db_Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<ClienteTipo> ClienteTipoes { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Marca> Marcas { get; set; }
        public DbSet<Ponteiro> Ponteiroes { get; set; }
        public DbSet<Produto> Produtoes { get; set; }
        public DbSet<OrdemServico> OrdemServicoes { get; set; }
        public DbSet<AcessoWeb> AcessoWebs { get; set; }
        public DbSet<Instagram> Instagrams { get; set; }
    }
}
