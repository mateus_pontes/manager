//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Manager.Painel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Empresa
    {
        public Empresa()
        {
            this.Usuarios = new HashSet<Usuario>();
            this.ClienteTipoes = new HashSet<ClienteTipo>();
            this.Clientes = new HashSet<Cliente>();
            this.Marcas = new HashSet<Marca>();
            this.Produtoes = new HashSet<Produto>();
            this.OrdemServicoes = new HashSet<OrdemServico>();
            this.AcessoWebs = new HashSet<AcessoWeb>();
            this.Instagrams = new HashSet<Instagram>();
        }
    
        public int Codigo { get; set; }
        public string NomeEmpresa { get; set; }
        public string Cnpj { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Cep { get; set; }
        public string Estado { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Responsavel { get; set; }
        public string Site { get; set; }
        public Nullable<System.DateTime> DtCadastro { get; set; }
        public bool Liberado { get; set; }
        public bool Master { get; set; }
    
        public virtual ICollection<Usuario> Usuarios { get; set; }
        public virtual ICollection<ClienteTipo> ClienteTipoes { get; set; }
        public virtual ICollection<Cliente> Clientes { get; set; }
        public virtual ICollection<Marca> Marcas { get; set; }
        public virtual Ponteiro Ponteiro { get; set; }
        public virtual ICollection<Produto> Produtoes { get; set; }
        public virtual ICollection<OrdemServico> OrdemServicoes { get; set; }
        public virtual ICollection<AcessoWeb> AcessoWebs { get; set; }
        public virtual ICollection<Instagram> Instagrams { get; set; }
    }
}
