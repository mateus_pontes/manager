﻿$(function () {
    var produto = new (function () {
        this.model;
        this.callback;
        this.open = function (_id, callback) {
            produto.callback = (callback == null ? null : callback);

            $.ajax({
                type: 'POST',
                url: 'Produto/Edit',
                data: { id: _id },
                success: function (data) {
                    bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.salvar = function () {
            validaFormulario("frmCadCliente");

            var request = {
                Codigo: $("#Codigo").val(),                
                CodigoMarca: $("#CodigoMarca").val(),
                Descricao: $("#Descricao").val()
            };

            $.ajax({
                type: 'POST',
                url: 'Produto/Save',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    var html = [];
                    html.push("<td>" + data.Descricao + "</td><td>" + data.Marca + "</td>");
                    html.push("<td><center><a href='javascript:;' onclick='produto.open(" + data.Codigo + ");'>Editar</center></td></tr>");

                    if ($("#tr_" + data.Codigo).length > 0) {
                        $("#tr_" + data.Codigo).empty();
                        $("#tr_" + data.Codigo).html(html.join(''));
                    }
                    else {
                        $("#tblCliente").find("tbody").append("<tr id='tr_" + data.Codigo + "'>" + html.join('') + "</tr>");
                    }

                    alert("Cadastro realizado/alterado com sucesso!", "ok");
                    bootbox.hideAll()

                    if (produto.callback != null)
                        produto.callback(data);

                    produto.callback = null;
                }
            });
        },

        this.filter = function () {
            var request = {
                Nome: $("#NomeFiltro").val()
            }

            $.ajax({
                type: 'POST',
                url: 'Produto/SelectProduto',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    $("#resultFilter").empty();
                    $("#resultFilter").append(data);
                }
            });
        },        

        this.openFilter = function (callback) {
            produto.callback = callback;
            $.ajax({
                type: 'POST',                
                url: 'Produto/FilterDialog',
                success: function (data) {
                    bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.filter = function () {
            var request = {
                Descricao: $("#NomeFiltro").val()
            }

            $.ajax({
                type: 'POST',
                url: 'Produto/SelectProduto',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    $("#resultFilter").empty();
                    $("#resultFilter").append(data);
                }
            });
        },

        this.filterCallback = function (id) {
            if (produto.callback != null)
                produto.callback(id);

            bootbox.hideAll();
        },

        this.resultMarca = function (data) {
            $("#CodigoMarca").append($('<option>', { value: data.Codigo, text: data.Descricao }));

            alert("Cadastro realizado com sucesso", "ok");
        }
    })();
    window.produto = produto;
});