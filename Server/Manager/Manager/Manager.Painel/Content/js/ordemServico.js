﻿$(function () {
    var ordemServico = new (function () {
        this.model;
        this.cliente;
        this.acompanhamentos = [];
        this.open = function (_id) {
            $.ajax({
                type: 'POST',
                url: 'TipoCliente/Edit',
                data: { id: _id },
                success: function (data) {
                    tipoCliente.model = bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.resultCliente = function (cliente) {
            $("#NomeCodigo").val(cliente.Codigo);
            $("#NomeCliente").val(cliente.Nome);
            $("#Endereco").val(cliente.Endereco);
            $("#Numero").val(cliente.Numero);
            $("#Bairro").val(cliente.Bairro);
            $("#Cidade").val(cliente.Cidade);
            $("#UF").val(cliente.UF);
            $("#Telefone").val(cliente.Telefone);
            $("#Celular").val(cliente.Celular);
            $("#Email").val(cliente.Email);
            $("#Cpf").val(cliente.Cpf);

            ordemServico.cliente = cliente;
            $("#CodigoCliente").val(cliente.Codigo);            
        },

        this.resultProduto = function (produto) {
            $("#Aparelho").val(produto.Descricao);
            $("#CodigoProduto").val(produto.Codigo);
        }

        this.salvar = function () {

            validaFormulario("frmCadCliente");
            validaFormulario("frmDadosOS");

            var request = {                
                clienteParam: {
                    Codigo: $("#CodigoCliente").val(),
                    Nome: $("#NomeCliente").val(),
                    Endereco: $("#Endereco").val(),
                    Numero: $("#Numero").val(),
                    Bairro: $("#Bairro").val(),
                    Cidade: $("#Cidade").val(),
                    UF: $("#UF").val(),
                    Telefone: $("#Telefone").val(),
                    Celular: $("#Celular").val(),
                    Email: $("#Email").val(),
                    Cpf: $("#Cpf").val()
                },
                osParam: {
                    Codigo: $("#CodigoOS").val(),
                    CodigoProduto: $("#CodigoProduto").val(),
                    TipoServico: $("#TipoServico").val(),
                    OSFabricante: $("#OSFabricante").val(),
                    CodigoFabricante: $("#CodigoFabricante").val(),
                    Serial: $("#Serial").val(),
                    Data: $("#DataAbertura").val(),
                    Acompanhamento: $("#Acompanhamento").val()
                }
            }

            $.ajax({
                type: 'POST',
                url: 'OS/Save',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    if (data.success) {
                        $("#CodigoOS").val(data.OSCodigo);
                        bootbox.dialog({
                            message: "OS Nº " + data.OSNumero + " foi salvo com sucesso!",
                            title: "Ordem de Serviço",
                            buttons: {
                                success: {
                                    label: "Cadastrar nova OS",
                                    className: "btn-warning",
                                    callback: function () {
                                        location.reload();
                                    }
                                },
                                main: {
                                    label: "Continuar Editando",
                                    className: "btn-primary",
                                    callback: function () {
                                        return;
                                    }
                                }
                            }
                        });
                    }
                }
            });            
        },

        this.filter = function () {
            var request = {
                target: $("#cboOpcao").val(),
                value: $("#FiltroValor").val()
            }

            $.ajax({
                type: 'POST',
                url: 'OS/Filter',
                data: { request: JSON.stringify(request) },
                success: function (data) {                    
                    bootbox.dialog({
                        message: data
                    })
                }
            });
        },

        this.filterCallback = function (os){
            $.ajax({
                type: 'POST',
                url: 'OS/ObtemOS',
                data: { request: JSON.stringify(os) },
                success: function (data) {
                    var r = data;

                    $("#CodigoCliente").val(r.clienteParam.Codigo);
                    $("#NomeCliente").val(r.clienteParam.Nome);
                    $("#Endereco").val(r.clienteParam.Endereco);
                    $("#Numero").val(r.clienteParam.Numero);
                    $("#Bairro").val(r.clienteParam.Bairro);
                    $("#Cidade").val(r.clienteParam.Cidade);
                    $("#UF").val(r.clienteParam.UF);
                    $("#Telefone").val(r.clienteParam.Telefone);
                    $("#Celular").val(r.clienteParam.Celular);
                    $("#Email").val(r.clienteParam.Email);
                    $("#Cpf").val(r.clienteParam.Cpf);

                    $("#CodigoOS").val(r.osParam.Codigo);
                    $("#CodigoProduto").val(r.osParam.CodigoProduto);
                    $("#TipoServico").val(r.osParam.TipoServico);
                    $("#OSFabricante").val(r.osParam.OSFabricante);
                    $("#CodigoFabricante").val(r.osParam.CodigoFabricante);
                    $("#Serial").val(r.osParam.Serial);
                    $("#DataAbertura").val(r.osParam.Data);
                    $("#Acompanhamento").val(r.osParam.Acompanhamento);
                    $("#Aparelho").val(r.osParam.Produto);

                    bootbox.hideAll();
                }
            });
        }
    })();
    window.ordemServico = ordemServico;
});