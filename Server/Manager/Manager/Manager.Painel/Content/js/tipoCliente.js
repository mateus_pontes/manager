﻿$(function () {
    var tipoCliente = new (function () {
        this.model;
        this.open = function (_id) {
            $.ajax({
                type: 'POST',
                url: 'TipoCliente/Edit',
                data: { id: _id },
                success: function (data) {
                    tipoCliente.model = bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.salvar = function () {
            validaFormulario("frmCadTipoCliente");

            var request = {
                Codigo: $("#Codigo").val(),
                Descricao: $("#edtDescricao").val()
            };

            $.ajax({
                type: 'POST',
                url: 'TipoCliente/Save',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    var html = [];
                    html.push("<tr><td>" + data.Descricao + "</td>");
                    html.push("<td><center><a href='javascript:;' onclick='tipoCliente.open(" + data.Codigo + ");'>Editar</center></td></tr>");

                    if ($("#td_" + data.Codigo).length == 0)
                        $("#tblTipoCliente").find("tbody").append(html.join(''));
                    else
                        $("#td_" + data.Codigo).text(data.Descricao);

                    alert("Cadastro realizado/alterado com sucesso!", "ok");                    
                    bootbox.hideAll();
                }
            });
        }
    })();
    window.tipoCliente = tipoCliente;
});