﻿$(function () {
    var cliente = new (function () {
        this.model;
        this.callback;
        this.open = function (_id, callback) {
            cliente.callback = (callback == null ? null : callback);

            $.ajax({
                type: 'POST',
                url: 'Cliente/Edit',
                data: { id: _id },
                success: function (data) {
                    bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.salvar = function () {
            validaFormulario("frmCadCliente");

            var request = {
                Codigo: $("#Codigo").val(),                
                CodigoClienteTipo: $("#CodigoClienteTipo").val(),
                Nome: $("#Nome").val(),
                Cpf: $("#Cpf").val(),
                Endereco: $("#Endereco").val(),
                Numero: $("#Numero").val(),
                Complemento: $("#Complemento").val(),
                Bairro: $("#Bairro").val(),
                Cidade: $("#Cidade").val(),
                UF: $("#UF").val(),
                Cep: $("#Cep").val(),
                Telefone: $("#Telefone").val(),
                Celular: $("#Celular").val(),
                Email: $("#Email").val(),
                Site: $("#Site").val(),
                Pessoa: $("#Pessoa").val(),
                Observacao: $("#Observacao").val()
            };

            $.ajax({
                type: 'POST',
                url: 'Cliente/Save',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    var html = [];
                    html.push("<td>" + data.Nome + "</td><td>" + data.Telefone + "</td><td>" + data.Celular + "</td><td>" + data.Email + "</td><td>" + data.Cidade + "</td>");
                    html.push("<td><center><a href='javascript:;' onclick='cliente.open(" + data.Codigo + ");'>Editar</center></td></tr>");

                    if ($("#tr_" + data.Codigo).length > 0) {
                        $("#tr_" + data.Codigo).empty();
                        $("#tr_" + data.Codigo).html(html.join(''));
                    }
                    else {
                        $("#tblCliente").find("tbody").append("<tr id='tr_" + data.Codigo + "'>" + html.join('') + "</tr>");
                    }

                    alert("Cadastro realizado/alterado com sucesso!", "ok");
                    bootbox.hideAll()

                    if (cliente.callback != null)
                        cliente.callback(data);

                    cliente.callback = null;
                }
            });
        },

        this.filter = function () {
            var request = {
                Nome: $("#NomeFiltro").val()
            }

            $.ajax({
                type: 'POST',
                url: 'Cliente/SelectCliente',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    $("#resultFilter").empty();
                    $("#resultFilter").append(data);
                }
            });
        },        

        this.openFilter = function (callback) {
            cliente.callback = callback;
            $.ajax({
                type: 'POST',                
                url: 'Cliente/FilterDialog',
                success: function (data) {
                    bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.filterCallback = function (id) {
            if (cliente.callback != null)
                cliente.callback(id);

            bootbox.hideAll();
        }
    })();
    window.cliente = cliente;
});