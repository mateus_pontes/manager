﻿$(function () {
    var instagram = new (function () {
        this.url = "";
        this.filtro = "";
        this.initializeMap = function (_id, callback) {
            var mapOptions = {
                center: { lat: -14.890714896792126, lng: -55.47324702641367 },
                zoom: 4
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

            var input = /** @type {HTMLInputElement} */(
                            document.getElementById('filterEndereco'));

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);
        },

        this.selectOptions = function () {
            $("#opPerfil").hide();
            $("#opHastag").hide();
            $("#opLocal").hide();
            $("#multPerfil").hide();
            $("#map-canvas").hide();
            $("#preview").empty();
            $("#lblPreview").text("Preview");

            instagram.url = "";
            instagram.filtro = "";

            var cb = $("#cboOptions").val();

            $("#" + cb).show();

            if ($("#cboOptions").val() == "opLocal") {
                $("#map-canvas").show();
                instagram.initializeMap();
            }
        },

        this.filterEndereco = function () {
            instagram.filtro = $("#filterEndereco").val();

            if (instagram.filtro == "") {
                alert("Favor informar o filtro", "erro");
                return;
            }
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': instagram.filtro }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    $("body, html").animate({
                        scrollTop: $("#map-canvas").offset().top
                    }, 600);

                    var location = results[0].geometry.location;

                    var url = "https://api.instagram.com/v1/locations/search?lat=" + location.lat() + "&lng=" + location.lng() + "&distance=5000&access_token=367487558.1fb234f.208f03da1c274b1a99cd9e2090229494";
                    $.ajax({
                        url: url,
                        type: 'GET',
                        crossDomain: true,
                        dataType: 'jsonp',
                        success: function (response) {
                            //debugger;
                            var bounds = new google.maps.LatLngBounds();
                            $.each(response.data, function (key, value) {
                                var marker = new MarkerWithLabel({
                                    position: new google.maps.LatLng(value.latitude, value.longitude),
                                    labelContent: value.name,
                                    labelAnchor: new google.maps.Point(22, 0),
                                    labelClass: "labels",
                                    labelStyle: { opacity: 0.75 },
                                    instagram: value,
                                    draggable: true,
                                    map: map
                                });

                                google.maps.event.addListener(marker, 'click', function () {

                                    $("#lblPreview").text("Preview - " + this.instagram.name)

                                    instagram.url = "https://api.instagram.com/v1/locations/" + this.instagram.id + "/media/recent?access_token=367487558.1fb234f.208f03da1c274b1a99cd9e2090229494"
                                    alert(this.instagram.id);

                                    $.ajax({
                                        url: instagram.url,
                                        type: 'GET',
                                        crossDomain: true,
                                        dataType: 'jsonp',
                                        success: function (response) {
                                            instagram.results(response, true);
                                        },
                                        error: function () { alert('Failed!'); }
                                    });
                                });

                                bounds.extend(new google.maps.LatLng(value.latitude, value.longitude));
                            });
                            map.fitBounds(bounds);
                        },
                        error: function () { alert('Failed!'); }
                    });

                } else {
                    alert('Endereço Inválido: ' + status);
                }
            });
        }

        this.filterHastag = function () {
            instagram.filtro = $("#filterHastag").val();

            if (instagram.filtro == "") {
                alert("Favor informar o filtro", "erro");
                return;
            }

            instagram.filtro = instagram.filtro.replace("#", "");

            instagram.url = "https://api.instagram.com/v1/tags/" + instagram.filtro + "/media/recent?COUNT=30&access_token=367487558.1fb234f.208f03da1c274b1a99cd9e2090229494";

            $.ajax({
                url: instagram.url,
                type: 'GET',
                crossDomain: true,
                dataType: 'jsonp',
                success: function (response) {
                    instagram.results(response, false);
                },
                error: function () { alert('Failed!'); }
            });
        }

        this.filterPerfil = function () {
            instagram.filtro = $("#filterPerfil").val();

            if (instagram.filtro == "") {
                alert("Favor informar o filtro", "erro");
                return;
            }

            var url = "https://api.instagram.com/v1/users/search?q=" + instagram.filtro + "&count=8&access_token=367487558.1fb234f.208f03da1c274b1a99cd9e2090229494";

            $.ajax({
                url: url,                
                type: 'GET',
                crossDomain: true,
                dataType: 'jsonp',
                success: function (response) {
                    //debugger;

                    $("#multPerfil").empty();
                    $("#preview").empty();

                    $.each(response.data, function(key, value){
                        var html = [];
                        html.push("<div class=\"col-md-3 box-container ui-sortable\" style=\"cursor: pointer\" onclick=\"instagram.selectPerfil(this)\">");
                        html.push("<input type=\"hidden\" value=" + value.id + " class=\"idPerfil\" />");
                        html.push("<div class=\"box border gray\">");
                        html.push("<div class=\"box-title\"><h4><i class=\"fa fa-instagram\"></i>" + value.username + "</h4></div>");
                        html.push("<div class=\"box-body\"><center>");
                        html.push("<img src=" + value.profile_picture + " style=\"border-radius: 100px\" />");
                        html.push("<br /><br /><b>Nome: </b> " + value.full_name);
                        html.push("</center></div></div></div>");

                        $("#multPerfil").append(html.join(''));
                    });
                    
                    $("#multPerfil").show();
                },
                error: function () { alert('Failed!'); }
            });
        }

        this.selectPerfil = function (obj) {
            //debugger;

            var hasChecked = $(obj).find(".green").length > 0;

            $.each($(".box-container"), function (key, box) {
                $(box).find(".box").removeClass("green");
            })

            if (!hasChecked) {
                $(obj).find(".box").addClass("green");
                var id = $($(".box-container").find(".green").parent().find(".idPerfil")).val();

                instagram.url = "https://api.instagram.com/v1/users/" + id + "/media/recent/?COUNT=30&access_token=367487558.1fb234f.208f03da1c274b1a99cd9e2090229494";

                $.ajax({
                    url: instagram.url,
                    type: 'GET',
                    crossDomain: true,
                    dataType: 'jsonp',
                    success: function (response) {
                        instagram.results(response, false);
                    },
                    error: function () { alert('Failed!'); }
                });
            }
        },

        this.results = function (response, url) {            
            $("#preview").empty();

            $.each(response.data, function (key, value) {
                var html = [];
                html.push("<div style=\"float: left;\">");
                html.push("<img src=" + value.images.low_resolution.url + " style=\"float:left;\" />");
                html.push("<div style=\" width: 100%; height: 40px; background-image: url('" + __path + "content/images/bg-instagram.png" + "'); position: relative; top: 280px;\">");
                html.push("<div style=\" position: absolute; top: 10px; left: 5px; \">");
                html.push("<img src=" + value.user.profile_picture + " style=\"width: 28px;border-radius: 100px\" /> ");
                html.push("<font style=\"color:white\"><b>" + value.user.username + "</b></font>");
                html.push("</div></div></div>");

                $("#preview").append(html.join(''));
            });

            $("body, html").animate({
                scrollTop: $("#preview").offset().top
            }, 600);

            $("#preview").show();

            var token = guid();            

            var request = {
                token: token,
                url: instagram.url,
                filtro: instagram.filtro,
                tipo: $("#cboOptions").val()
            }

            $.ajax({
                type: 'POST',
                url: 'Instagram/Save',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    var iframe = __path + "public/instagram?token=" + data.Token;
                    $("#lblIFrame").val("<iframe src='" + iframe + "' width=\"100%\" style=\"min-height: 400px;\"></iframe>")
                }
            });
        }
    })();
    window.instagram = instagram;
});


function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}