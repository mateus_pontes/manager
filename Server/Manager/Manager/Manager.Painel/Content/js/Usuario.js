﻿$(function () {
    var usuario = new (function () {
        this.model;
        this.callback;
        this.open = function (_id, callback) {
            usuario.callback = (callback == null ? null : callback);

            $.ajax({
                type: 'POST',
                url: 'Usuario/Edit',
                data: { id: _id },
                success: function (data) {
                    bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.salvar = function () {
            //validaFormulario("frmCadUsuario");

            var request = {
                Codigo: $("#Codigo").val(),
                Nome: $("#Nome").val(),
                Senha: $("#Senha").val(),
                Email: $("#Email").val(),
                Perfil: $("#Perfil").val(),
                Liberado: $("#Liberado").val() == "1" ? true : false
            };

            $.ajax({
                type: 'POST',
                url: 'Usuario/Save',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    var html = [];

                    var lib;

                    if (data.Liberado) {
                        lib = "Sim";
                    }
                    else {
                        lib = "Não";
                    }

                    html.push("<td>" + data.Nome + "</td><td>" + data.Email + "</td><td>" + lib + "</td>");
                    html.push("<td><center><a href='javascript:;' onclick='usuario.open(" + data.Codigo + ");'>Editar</center></td></tr>");

                    if ($("#tr_" + data.Codigo).length > 0) {
                        $("#tr_" + data.Codigo).empty();
                        $("#tr_" + data.Codigo).html(html.join(''));
                    }
                    else {
                        $("#tblUsuario").find("tbody").append("<tr id='tr_" + data.Codigo + "'>" + html.join('') + "</tr>");
                    }

                    alert("Cadastro realizado/alterado com sucesso!", "ok");
                    bootbox.hideAll()

                    if (usuario.callback != null)
                        usuario.callback(data);

                    usuario.callback = null;
                }
            });
        },

        this.filter = function () {
            var request = {
                Nome: $("#NomeFiltro").val()
            }

            $.ajax({
                type: 'POST',
                url: 'Usuario/SelectCliente',
                data: { request: JSON.stringify(request) },
                success: function (data) {
                    $("#resultFilter").empty();
                    $("#resultFilter").append(data);
                }
            });
        },

        this.openFilter = function (callback) {
            usuario.callback = callback;
            $.ajax({
                type: 'POST',
                url: 'Usuario/FilterDialog',
                success: function (data) {
                    bootbox.dialog({
                        message: data
                    })
                }
            })
        },

        this.filterCallback = function (id) {
            if (usuario.callback != null)
                usuario.callback(id);

            bootbox.hideAll();
        }
    })();

    window.usuario = usuario;
});