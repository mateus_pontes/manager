﻿function alert(msg, type) {
    var _type = "";
    var mensagem = "";

    if (type == "ok") {
        mensagem = "<b>Sucesso</b><br><br>"
        _type = "success";
    }
    else if (type == "erro") {
        mensagem = "<b>Erro</b><br><br>"
        _type = "error";
    }
    else
        _type = "info";

    mensagem += msg;

    $.notify(msg, _type);
}


function validaFormulario(formulario) {
    var isValid = true;
    var sb = [];

    var qtdErros = 0;

    $.each($("#" + formulario).find(".validar"), function (key, control) {
        /*if ($(control).attr('class').indexOf("notnull") > -1) {
            $(control).css({ "border": " 1px solid #ccc" });
            $(control).removeAttr("title");
        }*/
        $(control).css({ "border": " 1px solid #ccc" });
        $(control).removeAttr("title");
    });

    $.each($("#" + formulario).find(".validar"), function (key, input) {

        if ($(input).attr("style").indexOf("display: none") > 0) {
            isValid = true;
            $(input).css({ "border": " 1px solid #ccc" });
            $(input).removeAttr("title");

        } else {

            if ($(input).attr('class').indexOf("notnull") > -1) {
                if (input.type != 'select-one') {
                    if ($(input).val() == "") {
                        isValid = false;
                        qtdErros++;
                        $(input).css({ "border": " 1px solid red" });
                        $(input).attr("title", "Campo não pode ficar vazio");
                    }
                } else {
                    if ($("#" + input.id + " option:selected").val() == "") {
                        isValid = false;
                        qtdErros++;
                        $(input).css({ "border": " 1px solid red" });
                        $(input).attr("title", "Campo não pode ficar vazio");
                    }
                }
            }

            if ($(input).attr('class').indexOf("email") > -1) {
                var filtro = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;

                if ($(input).val().trim().length > 0){
                    if (!filtro.exec($(input).val())){
                        isValid = false;
                        qtdErros++;
                        $(input).css({ "border": " 1px solid red" });
                        $(input).attr("title", "Email informado não esta em um formato válido");
                    }
                }
            }
        }
    });

    if (qtdErros > 0) {
        var tooltips = $("[title]").tooltip({
            position: {
                my: "left top",
                at: "right+5 top-5"
            }
        });

        sb.push("Favor verificar os campos em destaque!");
        throw new ValidaFormularioException(sb);
    } else {
        $(document).tooltip({
            disabled: true
        });
    }
}



function ValidaFormularioException(erros) {
    var message = "";
    $.each(erros, function (key, erro) {
        message += erro;
    });

    alert(message, "erro");
    return;
}

function applyMasks() {
    $.each(jQuery('input[mask=telefone]'), function (key, element) {
        $(element).unmask();
        $(element).mask("(99) 9999-999?9");
    });

    $.each(jQuery('input[mask=celular]'), function (key, element) {
        $(element).unmask();
        $(element).mask("(99) 99999-999?9");
    });

    $.each(jQuery('input[mask=cep]'), function (key, element) {
        $(element).unmask();
        $(element).mask("99999-999");
    });

    $.each(jQuery('input[mask=cpf]'), function (key, element) {
        $(element).unmask();
        $(element).mask("999.999.999-99");
    });

    $.each(jQuery('input[mask=cnpj]'), function (key, element) {
        $(element).unmask();
        $(element).mask("99.999.999/9999-99");
    });

    $.each(jQuery('input[mask=data]'), function (key, element) {
        $(element).unmask();
        $(element).mask("99/99/9999");
    });

    $.each(jQuery('input[mask=cpjcnpj]'), function (key, element) {
        $(element).keyup(function (event) {
            var target, caracteres, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            
            caracteres = target.value.replace(/\D/g, '');
            element = $(target);

            var count = caracteres.length;

            $(element).unmask();
            $(element).mask("99.999.999/9999-99");
        });
    });
}