﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers.Cadastros
{
    public class ClienteController : BaseController
    {
        //
        // GET: /Cliente/

        public ActionResult Index()
        {
            if (TempData["change"] != null)
            {
                ViewBag.change = TempData["change"].ToString();
            }

            var empresa = Context.getCodigoEmpresa();
            return View(db.Clientes.Where(s => s.CodigoEmpresa == empresa).ToList());
        }

        [HttpPost]
        public ActionResult Edit(int id = 0)
        {
            var codigoEmpresa = Context.getCodigoEmpresa();

            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                cliente = new Cliente();
                //return HttpNotFound();
            }

            #region tipo cliente
            var tiposCliente = new List<SelectListItem>();
            tiposCliente.Add(new SelectListItem() { Text = "<selecione>", Value = "" });
            foreach (var item in db.ClienteTipoes.Where(p => p.CodigoEmpresa == codigoEmpresa).ToList())
            {
                tiposCliente.Add(new SelectListItem() { Text = item.Descricao, Value = item.Codigo.ToString() });
            }

            if (cliente.Codigo == 0)
                ViewBag.CodigoClienteTipo = new SelectList(tiposCliente, "Value", "Text", "");
            else
                ViewBag.CodigoClienteTipo = new SelectList(tiposCliente, "Value", "Text", cliente.CodigoClienteTipo);
            #endregion

            #region uf
            var ufs = new List<SelectListItem>();
            ufs.Add(new SelectListItem() { Text = "<selecione>", Value = "" });
            foreach (var item in Context.getEstados())
            {
                ufs.Add(new SelectListItem() { Text = item.nome, Value = item.sigla });
            }

            if (cliente.Codigo == 0)
                ViewBag.UF = new SelectList(ufs, "Value", "Text", "");
            else
                ViewBag.UF = new SelectList(ufs, "Value", "Text", cliente.UF);
            #endregion

            #region uf
            var pessoas = new List<SelectListItem>();
            pessoas.Add(new SelectListItem() { Text = "<selecione>", Value = "" });
            pessoas.Add(new SelectListItem() { Text = "Pessoa Física", Value = "Física" });
            pessoas.Add(new SelectListItem() { Text = "Pessoa Jurídica", Value = "Jurídica" });

            if (cliente.Codigo == 0)
                ViewBag.Pessoa = new SelectList(pessoas, "Value", "Text", "");
            else
                ViewBag.Pessoa = new SelectList(pessoas, "Value", "Text", cliente.Pessoa);
            #endregion

            return PartialView(cliente);
        }

        [HttpPost]
        public JsonResult Save(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var model = Newtonsoft.Json.JsonConvert.DeserializeObject<Cliente>(request);
            model.CodigoEmpresa = Context.getCodigoEmpresa();

            if (!db.Clientes.Any(x => x.Codigo == model.Codigo && x.CodigoEmpresa == model.CodigoEmpresa))
            {
                model.DtCadastro = DateTime.Now;
                db.Clientes.Add(model);
            }
            else
                db.Entry(model).State = EntityState.Modified;

            db.SaveChanges();

            /*var result = new SaveResult();
            result.Codigo = model.Codigo;
            result.Nome = model.Nome;
            result.Telefone = model.Telefone;
            result.Celular = model.Celular;
            result.Email = model.Email;
            result.Cidade = model.Cidade;*/

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterDialog()
        {
            return PartialView();
        }

        public PartialViewResult SelectCliente(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var model = Newtonsoft.Json.JsonConvert.DeserializeObject<Cliente>(request);
            var codigoEmpresa = Context.getCodigoEmpresa();

            var query = db.Clientes.Where(p => p.CodigoEmpresa == codigoEmpresa);

            if (!model.Nome.Equals(""))
            {
                //query = query.Where(p => p.Nome.Contains(model.Nome));
            }

            var clientes = db.Clientes.SqlQuery(string.Format("SELECT * FROM Cliente WHERE CodigoEmpresa = {0} AND Nome like '%{1}%' ORDER BY Nome", codigoEmpresa, model.Nome)).ToList();
            return PartialView(clientes);
        }

        private class SaveResult
        {
            public int Codigo;
            public string Nome;
            public string Telefone;
            public string Celular;
            public string Email;
            public string Cidade;
        }

        private class SelectClienteResult
        {
            public string data;
            public string value;
        }
    }
}
