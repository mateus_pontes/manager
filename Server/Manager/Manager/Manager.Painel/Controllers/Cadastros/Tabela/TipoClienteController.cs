﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Manager.Painel.Models;

namespace Manager.Painel.Controllers.Cadastros.Tabela
{
    public class TipoClienteController : BaseController
    {
        private db_Entities db = new db_Entities();

        //
        // GET: /TipoCliente/

        public ActionResult Index()
        {
            var empresa = Context.getCodigoEmpresa();
            return View(db.ClienteTipoes.Where(s => s.CodigoEmpresa == empresa).ToList());
        }

        //
        // GET: /TipoCliente/Details/5

        public ActionResult Details(int id = 0)
        {
            ClienteTipo clientetipo = db.ClienteTipoes.Find(id);
            if (clientetipo == null)
            {
                return HttpNotFound();
            }
            return View(clientetipo);
        }

        //
        // GET: /TipoCliente/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TipoCliente/Create

        [HttpPost]        
        public ActionResult Create(ClienteTipo clientetipo)
        {
            if (ModelState.IsValid)
            {
                db.ClienteTipoes.Add(clientetipo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(clientetipo);
        }

        //
        // GET: /TipoCliente/Edit/5

        [HttpPost]
        public ActionResult Edit(int id = 0)
        {
            ClienteTipo clientetipo = db.ClienteTipoes.Find(id);
            if (clientetipo == null)
            {
                clientetipo = new ClienteTipo();
                //return HttpNotFound();
            }
            return PartialView(clientetipo);
        }

        //
        // POST: /TipoCliente/Edit/5

        [HttpPost]        
        public JsonResult Save(string request)
        {
            var codigoEmpresa = Context.getCodigoEmpresa();
            var transportador = Newtonsoft.Json.JsonConvert.DeserializeObject<ClienteTipo>(request);

            var model = db.ClienteTipoes.FirstOrDefault(x => x.Codigo == transportador.Codigo && x.CodigoEmpresa == codigoEmpresa);

            if (model == null) { model = new ClienteTipo(); }
            model.CodigoEmpresa = codigoEmpresa;
            model.Descricao = transportador.Descricao;

            if (model.Codigo == 0)
                db.ClienteTipoes.Add(model);
            else
                db.Entry(model).State = EntityState.Modified;

            db.SaveChanges();

            var result = new SaveResult();
            result.Codigo = model.Codigo;
            result.Desricao = model.Descricao;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private class SaveResult
        {
            public int Codigo;
            public string Desricao;
        }

        //
        // GET: /TipoCliente/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ClienteTipo clientetipo = db.ClienteTipoes.Find(id);
            if (clientetipo == null)
            {
                return HttpNotFound();
            }
            return View(clientetipo);
        }       

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}