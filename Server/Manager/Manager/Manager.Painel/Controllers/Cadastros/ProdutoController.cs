﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers.Cadastros
{
    public class ProdutoController : BaseController
    {
        //
        // GET: /Produto/

        public ActionResult Index()
        {
            if (TempData["change"] != null)
            {
                ViewBag.change = TempData["change"].ToString();
            }

            var empresa = Context.getCodigoEmpresa();
            return View(db.Produtoes.Where(s => s.CodigoEmpresa == empresa).ToList());
        }

        [HttpPost]
        public ActionResult Edit(int id = 0)
        {
            var codigoEmpresa = Context.getCodigoEmpresa();

            Produto produto = db.Produtoes.Find(id);
            if (produto == null)
            {
                produto = new Produto();
                //return HttpNotFound();
            }

            #region tipo cliente
            var marcas = new List<SelectListItem>();
            marcas.Add(new SelectListItem() { Text = "<selecione Marca>", Value = "" });
            foreach (var item in db.Marcas.Where(p => p.CodigoEmpresa == codigoEmpresa).ToList())
            {
                marcas.Add(new SelectListItem() { Text = item.Descricao, Value = item.Codigo.ToString() });
            }

            if (produto.Codigo == 0)
                ViewBag.CodigoMarca = new SelectList(marcas, "Value", "Text", "");
            else
                ViewBag.CodigoMarca = new SelectList(marcas, "Value", "Text", produto.CodigoMarca);
            #endregion

            return PartialView(produto);
        }

        [HttpPost]
        public JsonResult Save(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var model = Newtonsoft.Json.JsonConvert.DeserializeObject<Produto>(request);
            model.CodigoEmpresa = Context.getCodigoEmpresa();

            if (!db.Produtoes.Any(x => x.Codigo == model.Codigo && x.CodigoEmpresa == model.CodigoEmpresa))
            {
                model.DtCadastro = DateTime.Now;
                db.Produtoes.Add(model);
            }
            else
                db.Entry(model).State = EntityState.Modified;

            db.SaveChanges();

            var result = new SaveResult();
            result.Codigo = model.Codigo;
            result.Descricao = model.Descricao;
            result.Marca = db.Marcas.Find(model.CodigoMarca).Descricao;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterDialog()
        {
            return PartialView();
        }

        public PartialViewResult SelectProduto(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var model = Newtonsoft.Json.JsonConvert.DeserializeObject<Produto>(request);
            var codigoEmpresa = Context.getCodigoEmpresa();

            var query = db.Clientes.Where(p => p.CodigoEmpresa == codigoEmpresa);

            var produtos = db.Produtoes.SqlQuery(string.Format("SELECT * FROM Produto WHERE CodigoEmpresa = {0} AND Descricao like '%{1}%' ORDER BY Descricao", codigoEmpresa, model.Descricao)).ToList();
            return PartialView(produtos);
        }

        private class SaveResult
        {
            public int Codigo;
            public string Descricao;
            public string Marca;
        }
    }
}
