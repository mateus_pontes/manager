﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers.Cadastros
{
    public class MarcaController : BaseController
    {
        //
        // GET: /Marca/

        public ActionResult Index()
        {
            var empresa = Context.getCodigoEmpresa();
            return View(db.Marcas.Where(s => s.CodigoEmpresa == empresa).ToList());
        }

        [HttpPost]
        public ActionResult Edit(int id = 0)
        {
            Marca marca = db.Marcas.Find(id);
            if (marca == null)
            {
                marca = new Marca();
                //return HttpNotFound();
            }
            return PartialView(marca);
        }

        [HttpPost]
        public JsonResult Save(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var codigoEmpresa = Context.getCodigoEmpresa();
            var marca = Newtonsoft.Json.JsonConvert.DeserializeObject<Marca>(request);

            var model = db.Marcas.FirstOrDefault(x => x.Codigo == marca.Codigo && x.CodigoEmpresa == codigoEmpresa);

            if (model == null) { model = new Marca(); }
            model.CodigoEmpresa = codigoEmpresa;
            model.Descricao = marca.Descricao;

            if (model.Codigo == 0)
                db.Marcas.Add(model);
            else
                db.Entry(model).State = EntityState.Modified;

            db.SaveChanges();

            //var result = new SaveResult();
            //result.Codigo = model.Codigo;
            //result.Desricao = model.Descricao;

            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}
