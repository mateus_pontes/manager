﻿using Manager.Painel.Filters;
using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers
{
    [Compress]
    [Authenticaded]
    public class BaseController : Controller
    {
        public db_Entities db = new db_Entities();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
