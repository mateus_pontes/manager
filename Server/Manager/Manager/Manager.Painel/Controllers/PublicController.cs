﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers
{
    public class PublicController : Controller
    {
        private db_Entities db = new db_Entities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Instagram(string token)
        {
            var instagram = db.Instagrams.FirstOrDefault(x => x.Token == token);
            return View(instagram);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
