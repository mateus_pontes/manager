﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Manager.Painel.Models;

namespace Manager.Painel.Controllers
{
    public class UsuarioController : BaseController
    {
        private db_Entities db = new db_Entities();

        public ActionResult Index()
        {
            int codigoEmpresa = Context.getCodigoEmpresa();

            var usuarios = db.Usuarios.Where(u => u.CodigoEmpresa == codigoEmpresa);
            return View(usuarios.ToList());
        }

        [HttpPost]
        public ActionResult Save(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var model = Newtonsoft.Json.JsonConvert.DeserializeObject<Usuario>(request);
            model.CodigoEmpresa = Context.getCodigoEmpresa();

            if (!db.Usuarios.Any(x => x.Codigo == model.Codigo && x.CodigoEmpresa == model.CodigoEmpresa))
            {
                model.DtCadastro = DateTime.Now;
                db.Usuarios.Add(model);
            }
            else
                db.Entry(model).State = EntityState.Modified;

            db.SaveChanges();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Edit(int id = 0)
        {
            //var codigoEmpresa = Context.getCodigoEmpresa();

            var liberado = new List<SelectListItem>();
            liberado.Add(new SelectListItem() { Text = "<selecione>", Value = "" });
            liberado.Add(new SelectListItem() { Text = "Sim", Value = "1" });
            liberado.Add(new SelectListItem() { Text = "Não", Value = "0" });
            

            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                usuario = new Usuario();
                //return HttpNotFound();
            }

            ViewBag.Liberado = new SelectList(liberado, "Value", "Text", Convert.ToInt32(usuario.Liberado));

            return PartialView(usuario);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}