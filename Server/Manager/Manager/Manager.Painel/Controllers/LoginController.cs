﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers
{
    public class LoginController : Controller
    {
        private db_Entities db = new db_Entities();

        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Authentication(FormCollection form)
        {
            //return RedirectToAction("Home");

            string flogin = form["username"].ToString();
            string fsenha = form["password"].ToString();

            var user = db.Usuarios.Where(p => p.Email == flogin && p.Senha == fsenha).FirstOrDefault();

            if (user != null)
            {

                Session["User"] = user;

                var userModel = new UserModel();
                userModel.usuario = user;

                Context.NomeEmpresa = userModel.usuario.Empresa.NomeEmpresa;

                System.Web.HttpContext.Current.Session[UserModel.USER_SESSION_KEY] = userModel;

                if (!user.Empresa.Master)
                {
                    var acesso = new AcessoWeb();
                    acesso.CodigoEmpresa = user.CodigoEmpresa;
                    acesso.CodigoUsuario = user.Codigo;
                    acesso.Data = DateTime.Now;

                    db.AcessoWebs.Add(acesso);
                    db.SaveChanges();
                }

                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["MsgErro"] = "Usuário ou Senha inválidos";
                return View("index");
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public JsonResult UserFail()
        {
            return Json(new { message = "expired" }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public RedirectResult UserRedirect()
        {
            return Redirect("~/Login");
        }

        public RedirectResult Logout()
        {
            Session.Abandon();
            return UserRedirect();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
