﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers
{
    public class InstagramController : BaseController
    {
        //
        // GET: /Instagram/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Save(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var param = Newtonsoft.Json.JsonConvert.DeserializeObject<SaveParam>(request);
            var codigoEmpresa = Context.getCodigoEmpresa();

            var instagram = db.Instagrams.FirstOrDefault(p => p.CodigoEmpresa == codigoEmpresa);

            if (instagram == null) { instagram = new Instagram(); }
            instagram.CodigoEmpresa = codigoEmpresa;
            instagram.Filtro = param.filtro;
            instagram.Tipo = param.tipo;
            instagram.Token = param.token;
            instagram.Url = param.url;

            if (instagram.Codigo == 0)
                db.Instagrams.Add(instagram);
            else
                db.Entry(instagram).State = System.Data.EntityState.Modified;

            db.SaveChanges();

            return Json(instagram, JsonRequestBehavior.AllowGet);
        }

        private class SaveParam
        {
            public string token;
            public string url;
            public string filtro;
            public string tipo;
        }
    }
}
