﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Manager.Painel.Models;

namespace Manager.Painel.Controllers
{
    public class EmpresaController : BaseController
    {
        private db_Entities db = new db_Entities();

        //
        // GET: /Empresa/

        public ActionResult Index()
        {
            int codigoEmpresa = Context.getCodigoEmpresa();
            var empresas = db.Empresas.Where(e => e.Codigo == codigoEmpresa).FirstOrDefault();

            #region uf
            var ufs = new List<SelectListItem>();
            ufs.Add(new SelectListItem() { Text = "<selecione>", Value = "" });
            foreach (var item in Context.getEstados())
            {
                ufs.Add(new SelectListItem() { Text = item.nome, Value = item.sigla });
            }

            if (empresas.Codigo == 0)
                ViewBag.Estado = new SelectList(ufs, "Value", "Text", "");
            else
                ViewBag.Estado = new SelectList(ufs, "Value", "Text", empresas.Estado);
            #endregion

            return View(empresas);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empresa).State = EntityState.Modified;
                db.SaveChanges();

                ViewBag.mensagem = "Empresa atualizada com sucesso";

                return RedirectToAction("Index");
            }
            ViewBag.Codigo = new SelectList(db.Ponteiroes, "CodigoEmpresa", "CodigoEmpresa", empresa.Codigo);
            return View(empresa);
        }

        //
        // GET: /Empresa/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Empresa empresa = db.Empresas.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        //
        // POST: /Empresa/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Empresa empresa = db.Empresas.Find(id);
            db.Empresas.Remove(empresa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}