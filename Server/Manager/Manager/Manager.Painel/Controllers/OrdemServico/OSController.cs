﻿using Manager.Painel.Models;
using Manager.Painel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers.OrdemServico
{
    public class OSController : BaseController
    {
        //
        // GET: /OS/

        public ActionResult Index()
        {
            var os = new Manager.Painel.Models.OrdemServico();
            return View(os);
        }

        [HttpPost]
        public JsonResult Save(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;

            var param = Newtonsoft.Json.JsonConvert.DeserializeObject<SaveParameters>(request);
            var c = param.clienteParam;
            var ox = param.osParam;
            var codigoEmpresa = Context.getCodigoEmpresa();


            //Salva o Cliente
            var cliente = db.Clientes.FirstOrDefault(p => p.CodigoEmpresa == codigoEmpresa && p.Cpf == param.clienteParam.Cpf);

            if (cliente == null) { cliente = new Cliente(); }
            cliente.Bairro = c.Bairro;
            cliente.Celular = c.Celular;
            cliente.Cidade = c.Cidade;
            cliente.Email = c.Email;
            cliente.Endereco = c.Endereco;
            cliente.Nome = c.Nome;
            cliente.Numero = c.Numero;
            cliente.Telefone = c.Telefone;
            cliente.UF = c.UF;
            cliente.CodigoEmpresa = codigoEmpresa;
            cliente.Cpf = c.Cpf;

            if (cliente.Codigo == 0)
                db.Clientes.Add(cliente);
            else
                db.Entry(cliente).State = System.Data.EntityState.Modified;

            db.SaveChanges();

            var os = db.OrdemServicoes.FirstOrDefault(p => p.Codigo == ox.Codigo && p.CodigoEmpresa == codigoEmpresa);

            if (os == null) { os = new Manager.Painel.Models.OrdemServico(); }

            os.CodigoCliente = cliente.Codigo;
            os.CodigoEmpresa = codigoEmpresa;
            os.CodigoProduto = ox.CodigoProduto;
            os.CodigoUsuario = Context.getUsuario().Codigo;
            os.DtCadastro = DateTime.Now;
            os.DtEntrada = Convert.ToDateTime(ox.Data);
            os.NumeroSerie = ox.Serial;            
            os.OSNumeroFabricante = ox.OSFabricante;
            os.Acompanhamento = ox.Acompanhamento;
            os.CodigoFabricante = ox.CodigoFabricante;
            os.TipoServico = ox.TipoServico;

            if (os.Codigo == 0)
            {
                var service = new PonteiroService();
                os.OSNumero = service.getPonteiroOS(codigoEmpresa);

                db.OrdemServicoes.Add(os);
            }
            else
                db.Entry(os).State = System.Data.EntityState.Modified;

            db.SaveChanges();

            var r = new OSResult();
            r.success = true;
            r.OSNumero = os.OSNumero;
            r.OSCodigo = os.Codigo.ToString();
            r.CodigoCliente = os.CodigoCliente.ToString();

            return Json(r, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult Filter(string request)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var param = Newtonsoft.Json.JsonConvert.DeserializeObject<FilterParam>(request);
            var query = "";

            if (param.value != "")
            {
                if (param.target == "osnumero")
                    query = string.Format(" AND OSNumero = {0}", param.value);
                else if (param.target == "osfabricante")
                    query = string.Format(" AND OSNumeroFabricante LIKE '%{0}%'", param.value);
                else
                    query = string.Format(" AND cli.Nome LIKE '%{0}%'", param.value);
            }

            var os = db.OrdemServicoes.SqlQuery(string.Format(@"SELECT OrdemServico.* 
                                                                FROM OrdemServico 
                                                                JOIN Cliente cli on cli.Codigo = OrdemServico.CodigoCliente
                                                                WHERE OrdemServico.CodigoEmpresa = {0} AND 1=1 {1}", Context.getCodigoEmpresa(), query)).ToList();

            return PartialView(os);
        }

        [HttpPost]
        public JsonResult ObtemOS(string request)
        {
            var os = Newtonsoft.Json.JsonConvert.DeserializeObject<Manager.Painel.Models.OrdemServico>(request);
            var cliente = db.Clientes.Find(os.CodigoCliente);
            var produto = db.Produtoes.Find(os.CodigoProduto);

            var result = new SaveParameters();

            result.clienteParam = new ClienteParam();
            result.clienteParam.Bairro = cliente.Bairro;
            result.clienteParam.Celular = cliente.Celular;
            result.clienteParam.Cidade = cliente.Cidade;
            result.clienteParam.Codigo = cliente.Codigo;
            result.clienteParam.Email = cliente.Email;
            result.clienteParam.Endereco = cliente.Email;
            result.clienteParam.Nome = cliente.Nome;
            result.clienteParam.Numero = cliente.Numero;
            result.clienteParam.Telefone = cliente.Telefone;
            result.clienteParam.UF = cliente.UF;
            result.clienteParam.Cpf = cliente.Cpf;

            result.osParam = new OSParam();
            result.osParam.Acompanhamento = os.Acompanhamento;
            result.osParam.Codigo = os.Codigo;
            result.osParam.CodigoFabricante = os.CodigoFabricante;
            result.osParam.CodigoProduto = produto.Codigo;
            result.osParam.Data = os.DtEntrada.Value.ToShortDateString();
            result.osParam.OSFabricante = os.OSNumeroFabricante;
            result.osParam.Produto = produto.Descricao;
            result.osParam.Serial = os.NumeroSerie;
            result.osParam.TipoServico = os.TipoServico;

            return Json(result, JsonRequestBehavior.AllowGet);

        }

        private class FilterParam
        {
            public string target;
            public string value;
        }

        private class OSResult
        {
            public Boolean success;
            public string OSNumero;
            public string OSCodigo;
            public string CodigoCliente;
        }

        private class SaveParameters
        {
            public ClienteParam clienteParam;
            public OSParam osParam;
            public List<AcompanhamentosParam> acompanhamentosParam;
        }

        private class ClienteParam
        {
            public int Codigo;
            public string Nome;
            public string Endereco;
            public string Numero;
            public string Bairro;
            public string Cidade;
            public string UF;
            public string Telefone;
            public string Celular;
            public string Email;
            public string Cpf;
        }

        private class OSParam
        {
            public int Codigo;
            public int CodigoProduto;
            public string Produto;
            public string TipoServico;
            public string OSFabricante;
            public string CodigoFabricante;
            public string Serial;
            public string Data;
            public string Acompanhamento;
        }

        private class AcompanhamentosParam
        {
            public string Codigo;
            public string Texto;
        }
    }
}
