﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Controllers.OrdemServico
{
    public class OSConsultaController : Controller
    {
        public db_Entities db = new db_Entities();

        //
        // GET: /OSConsulta/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Consulta(string doc, string codEmp)
        {

            var result = new List<OS>();
            int _codEmp = Convert.ToInt32(codEmp);

            var cliente = db.Clientes.Where(p => p.CodigoEmpresa == _codEmp && p.Cpf == doc).FirstOrDefault();

            if (cliente != null)
            {
                var OSs = db.OrdemServicoes.Where(p => p.CodigoEmpresa == _codEmp && p.CodigoCliente == cliente.Codigo).OrderByDescending(x => x.Codigo).ToList();

                foreach (var os in OSs)
                {
                    result.Add(new OS()
                    {
                        acompanhamento = os.Acompanhamento,
                        dataAbertura = (os.DtEntrada.HasValue ? os.DtEntrada.Value.ToShortDateString() : ""),
                        dataEntrega = (os.DtEntrega.HasValue ? os.DtEntrega.Value.ToShortDateString() : ""),
                        nomeCliente = cliente.Nome,
                        osNumero = os.OSNumero.PadLeft(10, '0')
                    });
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private class OS
        {
            public string osNumero;
            public string dataAbertura;
            public string dataEntrega;
            public string acompanhamento;
            public string nomeCliente;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
