﻿using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Manager.Painel.Services
{
    public class PonteiroService
    {
        internal string getPonteiroOS(int codigoEmpresa)
        {
            var numero = 0;

            using(var ctx = new db_Entities())
            {
                var ponteiro = VerificaCadPonteiro(codigoEmpresa, ctx);
                numero = (int)++ponteiro.OS;

                ctx.Entry(ponteiro).State = System.Data.EntityState.Modified;
                ctx.SaveChanges();
            }

            return numero.ToString().PadLeft(10, '0');
        }

        private Ponteiro VerificaCadPonteiro(int codigoEmpresa, db_Entities ctx)
        {
            var ponteiro = ctx.Ponteiroes.FirstOrDefault(x => x.CodigoEmpresa == codigoEmpresa);

            if (ponteiro == null)
            {
                ponteiro = new Ponteiro();
                ponteiro.CodigoEmpresa = codigoEmpresa;
                ponteiro.OS = 0;

                ctx.Ponteiroes.Add(ponteiro);
                ctx.SaveChanges();
            }

            return ponteiro;
        }
    }
}