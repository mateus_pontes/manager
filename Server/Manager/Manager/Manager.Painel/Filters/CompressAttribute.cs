﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Manager.Painel.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class CompressAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //HttpResponse Response = HttpContext.Current.Response;

            //if (IsGZipSupported())
            //{
            //    string AcceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];

            //    if (AcceptEncoding.Contains("gzip"))
            //    {
            //        Response.Filter = new System.IO.Compression.GZipStream(Response.Filter,
            //                                    System.IO.Compression.CompressionMode.Compress);

            //        //Response.Headers["Content-Encoding"] = "gzip";
            //    }
            //    else
            //    {
            //        Response.Filter = new System.IO.Compression.DeflateStream(Response.Filter,
            //                                    System.IO.Compression.CompressionMode.Compress);

            //        Response.Headers["Content-Encoding"] = "deflate";
            //    }
            //}
        }

        public static bool IsGZipSupported()
        {
            string AcceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];

            if (!string.IsNullOrEmpty(AcceptEncoding) && (AcceptEncoding.Contains("gzip") || AcceptEncoding.Contains("deflate")))
                return true;

            return false;
        }
    }
}