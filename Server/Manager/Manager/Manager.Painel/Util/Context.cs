﻿using Manager.Painel.Controllers;
using Manager.Painel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Manager.Painel
{
    public static class Context
    {
        public static string NomeEmpresa;
        public static int getCodigoEmpresa()
        {
            return UserModel.AuthenticatedUser.usuario.CodigoEmpresa;
        }

        public static Usuario getUsuario()
        {
            return UserModel.AuthenticatedUser.usuario;
        }



        public static List<UF> getEstados()
        {
            List<UF> estados = new List<UF>();

            estados.Add(new UF() { sigla = "SP", nome = "São Paulo" });
            estados.Add(new UF() { sigla = "AC", nome = "Acre" });
            estados.Add(new UF() { sigla = "AL", nome = "Alagoas" });
            estados.Add(new UF() { sigla = "AP", nome = "Amapá" });
            estados.Add(new UF() { sigla = "AM", nome = "Amazonas" });
            estados.Add(new UF() { sigla = "BA", nome = "Bahia" });
            estados.Add(new UF() { sigla = "CE", nome = "Ceará" });
            estados.Add(new UF() { sigla = "DF", nome = "Distrito Federal" });
            estados.Add(new UF() { sigla = "ES", nome = "Espírito Santo" });
            estados.Add(new UF() { sigla = "GO", nome = "Goiás" });
            estados.Add(new UF() { sigla = "MT", nome = "Mato Grosso" });
            estados.Add(new UF() { sigla = "MS", nome = "Mato Grosso do Sul" });
            estados.Add(new UF() { sigla = "MA", nome = "Maranhão" });
            estados.Add(new UF() { sigla = "MG", nome = "Minas Gerais" });
            estados.Add(new UF() { sigla = "PA", nome = "Pará" });
            estados.Add(new UF() { sigla = "PB", nome = "Paraiba" });
            estados.Add(new UF() { sigla = "PR", nome = "Paraná" });
            estados.Add(new UF() { sigla = "PE", nome = "Pernambuco" });
            estados.Add(new UF() { sigla = "PI", nome = "Piauí" });
            estados.Add(new UF() { sigla = "RJ", nome = "Rio de Janeiro" });
            estados.Add(new UF() { sigla = "RN", nome = "Rio Grande do Norte" });
            estados.Add(new UF() { sigla = "RS", nome = "Rio Grande do Sul" });
            estados.Add(new UF() { sigla = "RO", nome = "Rondônia" });
            estados.Add(new UF() { sigla = "RR", nome = "Roraima" });
            estados.Add(new UF() { sigla = "SC", nome = "Santa Catarina" });            
            estados.Add(new UF() { sigla = "SE", nome = "Sergipe" });
            estados.Add(new UF() { sigla = "TO", nome = "Tocantins" });

            return estados;
        }

        public static string nextNumberOS(db_Entities db)
        {
            var ponteiro = db.Ponteiroes.FirstOrDefault(p => p.CodigoEmpresa == getCodigoEmpresa());

            if (!ponteiro.OS.HasValue)
                ponteiro.OS = 0;

            ponteiro.OS++;

            db.Entry(ponteiro).State = System.Data.EntityState.Modified;
            db.SaveChanges();

            return ponteiro.OS.Value.ToString().PadLeft(10, '0');
        }

        public static Cliente getCliente(int? codigoCliente)
        {
            Cliente cliente;
            using (var ctx = new db_Entities())
            {
                cliente = ctx.Clientes.Find(codigoCliente);
            }

            return cliente;
        }
    }
}